/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Start up code provided by Paul Miller
***********************************************************************/
#include "board.h"

/**
 * @file board.c contains function implementations for managing the @ref
 * game_board
 **/

/**
 * @param board the game board to display
 **/
void init_board(game_board board)
{
        /* Declare loop counter */
        int y;
        
        /* Init board to 0 values */
        memset(board, 0, sizeof(board[0][0]) * BOARDWIDTH * BOARDHEIGHT);

        /* Loop over values in array */
        for(y = 0; y < BOARDHEIGHT; ++y)
        {
                int x;
                for(x = 0; x < BOARDWIDTH; ++x)
                {
                        /* Set all values to C_EMPTY */
                        board[y][x] = C_EMPTY;
                }
        }
}

/**
 * @param board the game board to display
 **/
void display_board(game_board board)
{
        printf("\n|---------------|\n");
        printf("|   | A | B | C |\n");
        printf("|---------------|\n");
        printf("| 1 | %c | %c | %c |\n", board[0][0], board[0][1], board[0][2]);
        printf("|---------------|\n");
        printf("| 2 | %c | %c | %c |\n", board[1][0], board[1][1], board[1][2]);
        printf("|---------------|\n");
        printf("| 3 | %c | %c | %c |\n", board[2][0], board[2][1], board[2][2]);
        printf("|---------------|\n");
        return;
}
