/***********************************************************************
 * CPT220 - Programming in C
 * Study Period 4 2015 Assignment #1 
 * Full Name        : Luke Warren
 * Student Number   : s3409172
 * Start up code provided by Paul Miller
 ***********************************************************************/
#include "main.h"

int main(void)
{
        /* variable that holds the menu choice selected by the user */
        enum menuchoice choice;
        /* scoreboard where the most recent games played is stored */
        score scoreboard[NUM_SCORES];
        /* structs representing the data for the human and
         * computer players */
        struct player computer, human;
        /* a pointer to the winner - this will be NULL if the game was a 
         * draw 
         * or the human player chose to quit the game half way through
         */
        struct player *winner;

        /* initialise the scoreboard */
        init_scoreboard(scoreboard);
        
        /* Stay in game loop until user elects to quit */
        while(choice != QUIT)
        {
                /* Get input from the user and trigger next step */
                get_menu_input(&choice);
                switch(choice)
                {
                        case PLAY:
                                /* User wants to Play a game */
                                printf("You selected Play Game...\n");
                                winner = play_game(&human, &computer);
                                if(winner != NULL)
                                {
                                        /* Insert Winner into score board */
                                        if(add_to_scoreboard(scoreboard, winner))
                                        {
                                                printf("Winner added to scoreboard");
                                        }
                                        else
                                        {
                                                printf("Name dropped off of scoreboard");
                                        }
                                }
                                break;
                        case SCORE:
                                /* User wants to Display the high scores */
                                printf("You selected show high scores\n");
                                display_scores(scoreboard);
                                break;
                        case QUIT:
                                /* User decided to exit  */
                                printf("\nQuiting...\n");
                                break;
                }
        }
        return EXIT_SUCCESS;
}
