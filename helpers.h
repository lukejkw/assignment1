/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Luke Warren
* Student Number   : s3409172
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "shared.h"

/** @file helpers.h contains the constants and data structure definitions
 * required for any functions defined in @ref helpers.c
 **/ 
 
#ifndef HELPERS_H 
#define HELPERS_H

#define MENUINPUTLENGTH 1
#define MAXUSERNAMELENGTH 55
#define EXTRACHARS 2
#define MINUSERNAMELENGTH 2
#define MAXCOORDLENGTH 3
/* ------------------------------------------------------------------------
 * Enumerations 
 * -----------------------------------------------------------------------*/

/**
 * enumeration that represents the various outcomes of an i/o operation
 **/
enum input_result
{
        /**
         * the input/output operation failed
         **/
        FAILURE,

        /**
         * the input/ouput operation succeeded
         **/
        SUCCESS,

        /**
         * the user pressed ctrl-d or 'enter' at the beginning of a line
         **/
        RTM
};

/*
 * Enum to represent the 3 different menu options
 */
enum menuchoice
{
        PLAY = 1,
        SCORE = 2,
        QUIT = 3
};

/* ------------------------------------------------------------------------
 *  Define function prototypes
 * ----------------------------------------------------------------------*/

/* Function to get user input */
enum input_result get_menu_input(enum menuchoice *);

/* Function to check for buffer overflow */
BOOLEAN check_buffer_overflow(const char);

/* Function to get and set the player's user name */
enum input_result get_username();

/* Function to prompt for and get coord from a user */
enum input_result get_user_coords(int *, int *);

/* Function to convert numbers to a value in range */
enum input_result convert_to_num(char *, int *, int, int);

#endif
