/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Start up code provided by Paul Miller
***********************************************************************/
#include "player.h"

/**
 * @file player.c contains functions to manage the @ref player data structure
 * which represents either a @ref HUMAN or @ref COMPUTER player
 **/

/**
 * @param human a pointer to the human player's struct @param token a pointer
 * to where a copy of the human player's token is stored so it can be accessed
 * later @return a result status indicating whether i/o was successful or not
 * or if the user pressed enter or ctrl-d on an empty line.
 **/
enum input_result init_human_player(struct player * human, enum cell * humanToken)
{
        /* Declare Variables */
        BOOLEAN isNought;
        enum input_result methodResult;

        /* Get and set user's name */ 
        methodResult = get_username(human-> name);
        
        /* Check for RTM - user wants to exit */
        if(methodResult == RTM)
                return RTM;

        printf("\nHi %s! Good Luck\n", human->name); /* Greet- why not? */

        /* Set random player token */
        isNought = (int)(rand() % 2);
        if(isNought)
        {
                *humanToken = C_NOUGHT;
                printf("%s assigned nought token. %s gets to start.\n", 
                        human->name,
                        human->name);
        }
        else
        {
                *humanToken = C_CROSS;
                printf("%s assigned cross token. Computer gets to start.\n", 
                        human->name);

        }
        /* Assign user token */
        human->token = *humanToken;

        /* Set player type */
        human->type = HUMAN;
        
        /* Randomly set Token type */
        return SUCCESS;
}

/**
 * @param computer a pointer to the computer player struct @param token the
 * token for the human player - the computer one needs to be the opposite
 * @return an indication of i/o success or failure which in this case can be
 * safely ignored as there is no i/o. It is only part of the interface so that
 * human and computer functions are called in a uniform way.
 **/
enum input_result init_computer_player(struct player * computer, enum cell token)
{
        /* Set comp user's name */
        strcpy(computer->name, "Computer");

        /* Set player type */
        computer->type = COMPUTER;
        
        /* Set opposite player token */
        if(token == C_CROSS)
                computer->token = C_NOUGHT;
        else
                computer->token = C_CROSS;

        /* Randomly set Token type */
        return SUCCESS;
}

/**
 * @param player the player to display the name of @return a pointer to the
 * player's name
 **/
const char * player_to_string(const struct player* player)
{
        return player->name;
}

/**
 * @param board the gameboard to insert the token into @param player a pointer
 * to the player struct that represents the current player.  @return a value
 * indicating whether any i/o operations were successful
 **/
enum input_result take_turn(game_board board, struct player * player)
{
        enum input_result methodResult;

        BOOLEAN coordsValid = FALSE;
        /* Stay in loop until valid coords or RTM have been entered */
        while(!coordsValid)
        {
                int x, y;
                /* Check player type */
                if(player->type == COMPUTER)
                {
                        /* Get random coords */
                        x = get_random_coord();
                        y = get_random_coord();
                }
                else
                {
                        /* Get user's valid selection or exit */
                        methodResult = get_user_coords(&x, &y);
                        if(methodResult == RTM)
                                return RTM;
                }

                /* Try and place token */
                if(board[y][x] == C_EMPTY)
                {
                        /* Coords valid! */
                        board[y][x] = player->token;
                        /* Show user the coords */ 
                        printf("%s moved to %s,%d \n", 
                                player->name, 
                                get_x_name(x),
                                y+1);
                        coordsValid = TRUE;
                }
                else
                {
                        /* Display message for human players */
                        if(player->type == HUMAN)
                                printf("ERROR: A player already has a token in that spot\n");
                        continue;
                }
        }
        return SUCCESS;
}

/*
 * Function to get random coord in range of game_board
 */
int get_random_coord()
{
        int coord;
        /* Get divisor value */
        int divisor = RAND_MAX/BOARDWIDTH;/* Could use board height also */
        
        /* Loop until we find a valid coord */
        do
        {
                /* Get possible coord in range */
                coord = rand() / divisor;
        } while (coord > BOARDWIDTH);
        
        /* We found a coord in range */
        return coord;
}

/* 
 * Function to get named column based on the value of x
 */
char * get_x_name(int x)
{
        /* Return letter corres. to x int value */
        switch(x)
        {
                case 0:
                        return "A";
                case 1:
                        return "B";
                case 2:
                        return "C";
                default:
                        return "?"; /* If value not found - return "?" */
        }
}

