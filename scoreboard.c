/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Luke Warren
* Student Number   : s3409172
* Start up code provided by Paul Miller
***********************************************************************/
#include "scoreboard.h"

/**
 * @file scoreboard.c contains the implementation of functions to manage the
 * scoreboard
 **/

/**
 * @param scoreboard the scoreboard to initialise to an empty board
 **/
void init_scoreboard(score * scoreboard)
{
        /* Create null object for score */
        struct player nullScore;
        size_t i;
        nullScore.token = C_INVALID;
        /* Initialize values in array */
        for(i=0; i < NUM_SCORES; i++)
        {
                scoreboard[i] = nullScore;
        }
}

/**
 * @param scoreboard the scoreboard to add the score to
 * @param newscore the new score to add too the scoreboard
 * @return TRUE when a score is successfully added and FALSE otherwise
 **/
BOOLEAN add_to_scoreboard(score * scoreboard, const score* newscore)
{
        /* Declare variables */
        size_t outer, inner, lowAlphaIndex;
        BOOLEAN inserted = FALSE;
        lowAlphaIndex = 0;

        /* 
         * Insert new score 
         */
        /* Check if last place in array is null */
        if(scoreboard[NUM_SCORES-1].token != C_INVALID)
        {
                /* If not - check if new score is 
                 * low aplhabetically than the current last placed score */
                if(strcmp(scoreboard[NUM_SCORES-1].name, newscore->name) > 0)
                {
                        scoreboard[NUM_SCORES-1] = *newscore;
                        inserted = TRUE;
                }
        }
        else
        {
                /* Insert new score */
                scoreboard[NUM_SCORES] = *newscore;
                inserted = TRUE;
        }
        
        /* Iterate over scores */
        for(outer=0; outer < NUM_SCORES; ++outer)
        {      
                /* Search over array for largest */
                for(inner = outer; inner < NUM_SCORES; ++inner)
                {
                        /* Compare */
                        if(strcmp(
                                scoreboard[lowAlphaIndex].name, 
                                scoreboard[inner].name) > 0)
                        {
                                /* Set smaller */
                                lowAlphaIndex = inner;
                        }
                }

                /* Swap */
                swap(&scoreboard[outer], &scoreboard[lowAlphaIndex]);
        }
        return inserted;
}

/**
 * @param scoreboard the scoreboard to display
 **/
void display_scores(score * scoreboard)
{
        int x;

        /* Print menu */
        printf("\nTictactoe Winners\n");
        printf("-----------------\n");
        printf("Name                   | Player Type       | Token Type\n");
        printf("-------------------------------------------------------\n");

        /* Start loop over scoreboard struc instances */
        for(x = 0; x < NUM_SCORES ;++x)
        {
                if(scoreboard[x].token != C_INVALID)
                {
                        /* Declare variables to hold names for types */
                        char typeName[TYPESPACE];
                        char tokenName[TOKENSPACE];
                        char name[NAMESPACE];
                        
                        strcpy(name, scoreboard[x].name);

                        /* Print name and spaced to print str */
                        strcat(name, 
                                get_spaces(NAMESPACE-strlen(scoreboard[x].name)));
                        printf("%s", name);
                        printf( "|");
                
                        /* Get type name string */
                        if(scoreboard[x].type == HUMAN)
                                strcpy(typeName, "Human");
                        else
                                strcpy(typeName, "Computer");

                        /* Print */
                        strcat(typeName, 
                                get_spaces(TYPESPACE-strlen(typeName)));
                        printf("%s", typeName);
                        printf( "|");


                        /* Get Token type */
                        if(scoreboard[x].token == C_CROSS)
                                strcpy(tokenName, "Cross");
                        else
                                strcpy(tokenName, "Nought");

                        /* Print */
                        strcat(tokenName, 
                                 get_spaces(TOKENSPACE-strlen(tokenName)));
                        printf("%s\n", tokenName);
                }
        }
}

void swap(score * first, score * second)
{
        score temp = *first;
        *first = *second;
        *second = temp;
}

char * get_spaces(int numSpaces)
{
        /* Counter for loop */
        int x;
        /* Dynamically declare string */
        char * ary = malloc(sizeof(char) * numSpaces);
        
        /* Init array */
        memset(ary, 0, sizeof(char) * numSpaces);
        
        /* Add all the spaces */
        for(x = 0; x < numSpaces; ++x)
        {
                ary[x] = ' ';
        }
        /* Return initialised */
        return ary;
}
