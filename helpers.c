/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Luke Warren
* Student Number   : s3409172
* Start up code provided by Paul Miller
***********************************************************************/
#include "helpers.h"

/**
 * @file helpers.c contains all functions used in this program that don't
 * nicely fit into another module. For example, input/output functions and
 * validation functions.
 **/

/**
 * clears the input buffer when there is leftover input in the buffer. 
 * This function should only be called when there is leftover input in the 
 * incoming buffer. Please refer to the fgets manpage (man fgets) on the 
 * university linux servers.
 **/
static void read_rest_of_line(void)
{
        int ch;
        while(ch = getc(stdin), ch != '\n' && ch != EOF);
        clearerr(stdin);
}

/* Function to show the menu - cannot be used outside of helpers */
static void show_menu(void)
{
        /* Show main menu (Question 1)*/
        printf("\n\nWelcome to tictactoe\n");
        printf("--------------------\n");
        printf("1. Play Game\n");
        printf("2. Display High Scores\n");
        printf("3. Quit\n");
        printf("Please enter your choice: ");
}

/* 
 * Function to get menu input 
 */
enum input_result get_menu_input(enum menuchoice * pMenuChoice)
{
        /* Define variables */
        BOOLEAN success = FALSE;
        char line[MENUINPUTLENGTH + EXTRACHARS];
        int menuInt = 0;
        enum input_result methodResult = FAILURE;
        
        /* Stay in loop until user enters valid data */
        while(!success)
        {
                /* Declare convertion result var */
                enum input_result convResult;

                /* Show menu to prompt for user input*/
                show_menu();
                
                /* Get input chars - and check for escape character */
                if(fgets(line, MENUINPUTLENGTH + EXTRACHARS, stdin) == NULL)
                {
                        *pMenuChoice = QUIT;
                        return methodResult = RTM;
                }

                /* Check buffer overflow and continue if too long */
                if(check_buffer_overflow(line[strlen(line) - 1]))
                        continue;

                /* Replace new line with nul terminator */
                line[strlen(line) - 1] = 0;
                
                /* Try convert to number in range of menu(1-3)*/
                convResult = convert_to_num(line, &menuInt, 0, 4);
                if(convResult == FAILURE)
                        continue;
                
                /* 'Cast' to valid enum */
                switch(menuInt)
                {
                        case 1:
                                *pMenuChoice = PLAY;
                                methodResult = SUCCESS;
                                break;
                        case 2:
                                *pMenuChoice = SCORE;
                                methodResult = SUCCESS;
                                break;
                        case 3:
                                *pMenuChoice = QUIT;
                                methodResult = RTM;
                                break;
                        default:
                                /* Catch all if conversion fails */
                                printf("ERROR: Unable to map input to menu option\n");
                                continue;
                }
                
                /* Set method result */
                methodResult = SUCCESS;
                
                /* Set flag - break out of while */
                success = TRUE;
        }
        return methodResult;
}

/* 
 * Function to get and set the player's user name 
 */
enum input_result get_username(char * line)
{
        /* Define variables*/
        BOOLEAN success = FALSE;
        enum input_result methodResult = FAILURE;
        /*char line[MAXUSERNAMELENGTH + EXTRACHARS];*/
        
        /* Stay in prompt loop until valid data is supplied */
        while(!success)
        {
                /* Prompt user for name */
                printf("Please enter your first name: ");
                
                /* Get user's name and check for excape sequence */
                if(fgets(line, MAXUSERNAMELENGTH + EXTRACHARS, stdin) == NULL)
                        return RTM;
                               
                /* Check buffer overflow and continue if too long */
                if(check_buffer_overflow(line[strlen(line) - 1]))
                        continue;
                else if(strlen(line) < (EXTRACHARS + MINUSERNAMELENGTH))
                {
                        /* Check if name not too short */
                        printf("ERROR: Name too short (supplied: '%s')\n", line);
                        continue;
                }
                
                /* Kill new line character */
                line[strlen(line) - 1] = 0;
                
                methodResult = SUCCESS;
                
                /* Set success flag - breaks out of loop */
                success = TRUE;
        }
        return methodResult;
}

/* 
 * Function to get coords
 */
enum input_result get_user_coords(int * x, int * y)
{
        /* Define variables*/
        BOOLEAN success = FALSE;
        enum input_result methodResult = FAILURE;
        char line[MAXCOORDLENGTH + EXTRACHARS];
        char * pTok;
        char * delim = ",";

        /* Stay in prompt loop until valid data is supplied */
        while(!success)
        {
                /* Declare var for conversion */
                enum input_result convResult;

                /* Prompt user for name */
                printf("Please enter your Coordinates eg. A,1: ");
                
                /* Get user's name and check for excape sequence */
                if(fgets(line, MAXCOORDLENGTH + EXTRACHARS, stdin) == NULL)
                        return RTM;
                               
                /* Check buffer overflow and continue if too long */
                if(check_buffer_overflow(line[strlen(line) - 1]))
                        continue;
                else if(strlen(line) != (MAXCOORDLENGTH + 1))
                {
                        /* 
                         * Make sure that input is the right length 
                         * +1 for \n char*/
                        printf("Line Length: %d\n", strlen(line));
                        printf("ERROR: Invalid coords. Please try again.\n");
                        continue;
                }
                                
                
                /* Kill new line character */
                line[strlen(line) - 1] = 0;
                
                /* Get Tokenised str*/
                pTok = strtok(line, delim);
                
                /* Debug */
                printf("x: '%s'\n", pTok);

                /* Try convert first token to index */
                if(strcmp(pTok, "A") == 0)
                        *x = 0;
                else if(strcmp(pTok, "B") == 0)
                        *x = 1;
                else if(strcmp(pTok, "C") == 0)
                        *x = 2;
                else
                {
                        /* Unable to convert to index */
                        printf("ERROR: Invalid format \n");
                        continue;
                }
                
                /* Move to next token */
                pTok = strtok(NULL, delim);

                /* Try convert to number in range of board selection(1-3)*/
                convResult = convert_to_num(pTok, y, 0, 4);
                if(convResult == FAILURE)
                        continue;

                /* Conversion success - deduct 1 to make index */
                *y = *y - 1;
                
                /* Set success flag - breaks out of loop */
                methodResult = SUCCESS;
                success = TRUE;
        }
        return methodResult;
}

/*
 * Func to convert a user string to a number
 */ 
enum input_result convert_to_num(char * numStr, int * num, int min, int max)
{
        /* Declare vars */
        char * end;

        /* Convert string to int  */
        *num =  (unsigned)strtol(numStr, &end, 0);
                
        /* Ensure that user input was a number */
        if(*end)
        {
                printf("ERROR: Input not numeric\n\n");
                return FAILURE;
        }
                
        /* Ensure in range */
        if(*num <= min || *num >= max)
        {
                printf("ERROR: Selection out of range\n");
                return FAILURE;        
        }
        
        /* Passed validation */
        return SUCCESS;
}

/* 
 * Method to check for buffer overflow 
 */
BOOLEAN check_buffer_overflow(const char lastChar)
{
        if(lastChar != '\n')
        {
                /* There was an overflow! */
                printf("ERROR: Input too long\n");
                read_rest_of_line();
                return TRUE;
        }
        
        /* There was no overflow :)*/
        return FALSE;
}
