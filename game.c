/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Luke Warren
* Student Number   : s3409172
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"
#include "player.h"

/**
 * @file game.c contains the functions required for management of the game
 * while it is being played.
 **/

/**
 * @param human a pointer to the human player struct
 * @param computer a pointer to the computer player struct
 * @return a pointer to the winning player or NULL if there was no
 * winner
 **/
struct player * play_game(struct player * human, struct player * computer)
{
        /* Declare Variables */
        game_board board;
        enum cell humanToken;
        struct player ** currentPlayer, ** waitingPlayer;
        enum game_result result = GR_NOWINNER;
        enum input_result methodResult;

        /* Seed Rand() */
        srand(time(NULL));

        /* Init human player */
        methodResult = init_human_player(human, &humanToken);
        if(methodResult == RTM)
        {
                printf("User decided to quit \n");
                return NULL;
        }
        
        /* Init computer Player parsing in human token to set opposite */
        init_computer_player(computer, humanToken);
        
        /* Init gameboard to empty values */
        init_board(board);

        /* Set Starting player - noughts always start */
        if(humanToken == C_NOUGHT)
        {
                currentPlayer = &human;
                waitingPlayer = &computer;
        }
        else
        {
                currentPlayer = &computer;
                waitingPlayer = &human;
        }
        /* Start game loop */
        while(result == GR_NOWINNER)
        {
                /* Get current token */
                enum cell currentToken = (*currentPlayer)->token;
                
                /* Show game board to user and show current player */
                display_board(board);
                printf("The current player is %s\n", (*currentPlayer)->name); 

                /* Take turn current player*/
                methodResult = take_turn(board, *currentPlayer);
                if(methodResult == RTM)
                        return NULL;/* User wants to escape */

                /* Test for winner */
                result = test_for_winner(board, currentToken);
                if(result != GR_NOWINNER)
                        break;

                /* Swap players */
                swap_players(currentPlayer, waitingPlayer);
        }
        /* Check if game was won, if so return winner  */
        if(result == GR_DRAW)
        {
                printf("Game Drawn.\n");
                return NULL;
        }
        else
        {
                /* Show winner */
                printf("Game won by %s\n", (*currentPlayer)->name);                                return *currentPlayer;
        }
}

/**
 * @param first the first player passed in
 * @param second the second player passed in
 **/
void swap_players(struct player** first, struct player** second)
{
        /* Declare and set temp var to hold second person */
        struct player * temp = *second;
        temp = *first;
        *first = *second;
        *second = temp;
}

/**
 * @param board the game_board passed in
 * @param cur_token
 **/
enum game_result test_for_winner(game_board board, enum cell cur_token)
{
        /* Declare winner flag var */
        BOOLEAN foundWinner = FALSE;
        int x, y;

        /* Check for horizonital winner */
        for(y = 0; y < BOARDHEIGHT; ++y)
        {
                /* If three in a row and all three a real token */
                if(board[y][0] == cur_token 
                        && board[y][1] == cur_token
                        && board[y][2] == cur_token)
                {
                        /* Set winner */
                        foundWinner = TRUE;
                        break;
                }
        }
        /* Check if winner was found */
        if(!foundWinner)
        {
                /* Check for vertical winner */
                for(x = 0; x < BOARDWIDTH; ++x)
                {
                        if(board[0][x] == cur_token 
                                && board[1][x]== cur_token
                                && board[2][x] == cur_token)
                        {
                                foundWinner = TRUE;
                                break;
                        }
                }
        }
        /* Check if we have still not found a winner */
        if(!foundWinner)
        {
                /* Check diagonal winner */
                if(board[0][0] == cur_token
                        && board[1][1] == cur_token 
                        && board[2][2] == cur_token)
                                foundWinner = TRUE;
                else if(board[0][2] == cur_token
                        && board[1][1] == cur_token 
                        && board[2][0] == cur_token)
                                foundWinner = TRUE;
        }

        /* Check if winner found */
        if(foundWinner)
        {
                /* 
                 * Return nought or cross game result
                 * Based on who is the current player
                 */
                if(cur_token == C_NOUGHT)
                        return GR_NOUGHT;
                else
                        return GR_CROSS;
        }
        else
        {
                /* No winner has been found - check for draw */
                for(x = 0; x < BOARDWIDTH; ++x)
                {
                        for(y = 0; y < BOARDHEIGHT; ++y)
                        {
                                /* If we find an empty cell - keep playing */
                                if(board[y][x] == C_EMPTY)
                                        return GR_NOWINNER;
                        }
                }
                /* No empty spots or winners found - game drawn */
                return GR_DRAW;
        }
}


