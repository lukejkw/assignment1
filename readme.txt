/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Luke Warren
* Student Number   : s3409172
* Start up code provided by Paul Miller
***********************************************************************/

Known Bugs:

- Adding and displaying of scores in not currently functioning. I hope to get marks for my attempt and logic. Simply ran out of time

Assignment Late:

- This assignment is submitted late as approved by our email conversation below:

Paul Miller (RMIT)
11:17 (2 days ago)

to me 
Hi Luke

Sorry for the delay in response. 

That will be fine. 

Although I can offer no further extension beyond this. 

I am also concerned about the issue of cascading extensions - If you get extensions for both the first and the second assignment then you have less time for the third assignment which is a big one as it is worth 25% of the marks for the course. 

regards



Paul


On 15/01/16 05:18, Luke Warren wrote:
Hi Paul,

I have made a huge amount in progress in catching up on all the lecture videos and that which I have missed. I have done the first 3 questions on the assignment but I don't think that I will be ready for submission for tomorrow.

Please can you give me till Sunday afternoon to get it done. Hopefully then I will be able to start on assignment 2! I have been working through the night to get things completed for this and my other subject, but having a full time job obviously means that I can't work in the day.

Regards,
Luke

On 9 January 2016 at 07:15, Luke Warren <s3409172@student.rmit.edu.au> wrote:
Thank you so much Paul.

On 09 Jan 2016 5:24 AM, "Paul Miller" <paul.miller@rmit.edu.au> wrote:
Hi Luke

Ok.

I'll give you a five day extension but keep in contact with me and I'll be flexible if you need more time. 

As proof of your extension please insert this email in your readme file. 

As blackboard submissions will have closed by then, please reply to this email with your zip file to submit your assignment. 

Cheers

Paul Miller
Education Researcher
CPT220 - Programming In C - Instructor
School of CS& IT, RMIT
GPO Box 2476 or 124 La Trobe Street,
Melbourne, 3001

RMIT University acknowledges the Wurundjeri people of the Kulin Nations as the traditional owners of the land on which the University stands. RMIT University respectfully recognises Elders both past and present.

On 8 January 2016 at 22:52, Luke Warren <s3409172@student.rmit.edu.au> wrote:
Hi Paul,

I hope that you are well and had a good end of year.

You may have noticed that I have been absent from recent chat sessions. This has been due to me being in Mozambique for the last 2 weeks with no internet access! I expected to be able to buy a sim card and still work but that was not possible due to residency requirements. This has really set me back both in this subject and my other subject.

Is it possible to apply for an extension so late (I am literally typing this message from the border of south Africa and Mozambique)? 7 days will hopefully be enough for me to catch up all the work I have missed for both my subjects and finish the assignment which I admit I am not very far at all with.

If not, is it still possible to bail out of the subject for this semester? This is obviously not ideal but I have been doing very well (HDs) and want to continue my good marks.

Thanks in advance,
Luke
